# Script instalar [Odoo](https://www.odoo.com "Odoo's Homepage") 11 para ubuntu 16.04 & 18.04
###### con las dependencias y modulos nesesarios para facturacion electronica CHILE. Ademas implementa funcionalidades extras para Restaurant

Los modulos que se instalan son los siguientes:
* l10n_cl_chart_of_account de  [Konos](https://github.com/KonosCL)
* l10n_cl_dte_point_of_sale [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* l10n_cl_fe [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* payment_khipu [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* payment_webpay [Daniel Santibáñez Polanco](https://gitlab.com/dansanti)
* reporting-engine
* Script instalacion [Odoo](https://www.odoo.com "Odoo's Homepage") https://github.com/Yenthe666/InstallScript

Es super basico el script basicamente ejectuta otro script de [Yenthe666](https://github.com/Yenthe666/InstallScript) para instalacion odoo, clona los repositorios de los modulos mencionado e instala las dependecias para correecto funcionamiento de odoo y los modulos de facturacion electronica CHILENA

## Instalacion:

#### Instalar Full
```
sudo wget https://gitlab.com/fanguloa/odoo11_inst/raw/master/installscript_full.sh
sudo chmod +x installscript_full.sh
sudo ./installscript_full.sh
```
#### Instalar Resto
```
sudo wget https://gitlab.com/fanguloa/odoo11_inst/raw/master/installscript_resto.sh
sudo chmod +x installscript_resto.sh
sudo ./installscript_resto.sh
```
#### Instalar Retail
```
sudo wget https://gitlab.com/fanguloa/odoo11_inst/raw/master/installscript_retail.sh
sudo chmod +x installscript_retal.sh
sudo ./installscript_resto.sh
```

### Recuerden canmbiar Password en /etc/odoo-server.conf
```
sudo nano /etc/odoo-server.conf
``
